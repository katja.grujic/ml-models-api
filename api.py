from flask import Flask, request, jsonify
from sklearn.externals import joblib
import traceback
import pandas as pd
import numpy as np

app = Flask(__name__)

@app.route('/predict', methods=['POST'])
def predict():
    if (resolution_model and bitrate_model and stalling_model):
        try:
            resolution_json = request.get_json().get('resolution')
            resolution_query = pd.get_dummies(pd.DataFrame(resolution_json, index=[0]))
            resolution_prediction = resolution_model.predict(resolution_query)

            bitrate_json = request.get_json().get('bitrate')
            bitrate_query = pd.get_dummies(pd.DataFrame(bitrate_json, index=[0]))
            bitrate_prediction = bitrate_model.predict(bitrate_query)

            stalling_json = request.get_json().get('stalling')
            stalling_query = pd.get_dummies(pd.DataFrame(stalling_json, index=[0]))
            stalling_prediction = stalling_model.predict(stalling_query)

            return jsonify({'resolution': resolution_prediction[0], 'bitrate': bitrate_prediction[0], 'stalling': stalling_prediction[0]})

        except:
            return jsonify({'trace': traceback.format_exc()})
    else:
        return ('No models here to use')

if __name__ == '__main__':
    try:
        port = int(sys.argv[1]) # This is for a command-line input
    except:
        port = 12345 # If you don't provide any port the port will be set to 12345

    resolution_model = joblib.load("resolution.pkl")
    bitrate_model = joblib.load("bitrate.pkl")
    stalling_model = joblib.load("stalling.pkl")
    print ('Models loaded')

    app.run(port=port, debug=True)